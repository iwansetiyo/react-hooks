import React, { useState } from "react"
import logo from './logo.svg';
import './App.css';

const App = () => {
  const [ angka, setAngka ] = useState(0)
  const [ text, setText ] = useState("")
  const [ arr, setArr ] = useState([
  {
    id: 1,
    title: "this is title"
  }
  ])

  const [ obj, setObj ] = useState({
    name: "hiii"
  })

  const add = () => {
    setAngka(angka + 1)
  }
  const reduce = () => {
    setAngka(angka - 1)
  }

  return(
    <div>
      {angka}
      <button onClick={add}>add</button>
      <button onClick={reduce}>reduce</button>
    </div>
    )
}

export default App;
